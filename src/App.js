import './App.css';
import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Header from './component/Header';
import Restaurants from './component/Restaurants'
import Search from './component/Search';
import Footer from './component/Footer';
import Cart from './component/Cart';
import ViewRestaurant from './component/ViewRestaurant';
import Checkout from './component/Checkout';
import data from './data/product.json'
import validator from 'validator';
import toast, { Toaster } from 'react-hot-toast';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      mobileNumber: "",
      email: "",
      password: "",

      userNumber: "",
      userPassword: "",
      loginState: false,

      inCart: [],
      totalPrice: "",
      totalPay: "",
      taxes: "",
      totalItem: "",

      numberError: false,
      nameError: false,
      emailError: false,
      passError: false,
      signUpFlag: false,
      varifyError: false,
      loginPage: true,

      addressName: "",
      addressNumber: "",
      addressPincode: "",
      houseNo: "",
      areaName: "",
      landmark: "",

      userAddressName: "",
      userAddressNumber: "",
      userAddressPincode: "",
      userHouseNo: "",
      userAreaName: "",
      userLandmark: "",

      addressNameError: false,
      addressNumberError: false,
      addressPincodeError: false,
      houseError: false,
      areaNameError: false,
      addressFlag: false,


    }
  }

  handleChange = (event) => {
    let name = event.target.name
    this.setState({
      [name]: event.target.value.trim()
    })
  }

  signUp = () => {
    this.setState({
      userNumber: this.state.mobileNumber,
      userPassword: this.state.password,
    }
    )
  }

  notifyAddToCart = () => toast('Added to Cart.');
  notifyRemoveItem = () => toast('Item Removed.');
  notifyDecreaseQty = () => toast('Quantity Decreased.');

  login = () => {
    if (this.state.mobileNumber === this.state.userNumber &&
      this.state.password === this.state.userPassword) {
      this.setState({
        loginState: true,
        varifyError: false
      })
    } else {
      this.setState({
        loginState: false,
        varifyError: true
      })
    }
  }


  validation = () => {

    // let numberError = false;
    // if(!validator.isNumeric(this.state.mobileNumber)){
    //   numberError=true;
    // } else {
    //   if(!this.state.mobileNumber.length === 10){
    //   numberError=true;

    //   }
    // }

    
    this.setState({
      numberError: !validator.isNumeric(this.state.mobileNumber) || !validator.isLength(this.state.mobileNumber, {min:10, max:10}), 
      nameError: validator.isEmpty(this.state.name) || !validator.isAlpha(this.state.name),
      emailError: !validator.isEmail(this.state.email),
      passError: validator.isEmpty(this.state.password),

    }, () => {
      if (!this.state.loginPage) {
        if (!this.state.numberError && !this.state.nameError && !this.state.passError && !this.state.emailError) {
          this.signUp();
          this.setState({
            signUpFlag: true,
            mobileNumber: "",
            email: "",
            password: ""
          })
        }
      } else {
        if ((!this.state.numberError && !this.state.passError)) {
          this.login();
        }
      }
    })


  }

  loginToSignup = () => {
    if (this.state.loginPage === true) {
      this.setState({
        loginPage: false
      })
    } else {
      this.setState({
        loginPage: true
      })
    }
  }

  // ---------- Cart -----------------
  addToCart = (food) => {
    let foodAvailable = this.state.inCart.find((foodInCart) => {
      return foodInCart.id === food.id;
    })

    if (foodAvailable) {
      this.setState({
        inCart: this.state.inCart.map((foodInCart) => {
          if (foodInCart.id === food.id) {
            this.notifyAddToCart();
            return { ...foodInCart, quantity: foodInCart.quantity + 1 }
          } else {
            return foodInCart
          }
        })
      })

    } else {
      this.setState({
        inCart:
          [...this.state.inCart, { ...food, quantity: 1 }]
      },
        () => {
          this.notifyAddToCart();
        })
    }

  }

  increaseQty = (food) => {
    this.setState({
      inCart: this.state.inCart.map((foodInCart) => {
        if (foodInCart.id === food.id) {
          return { ...foodInCart, quantity: foodInCart.quantity + 1 }
        } else {
          return foodInCart
        }
      })
    })
  }

  decreaseQty = (food) => {
    let foodAvailable = this.state.inCart.find((foodInCart) => {
      return foodInCart.id === food.id;
    })

    if (foodAvailable.quantity === 1) {
      this.setState({
        inCart: this.state.inCart.filter((food) => {
          if (food.id === foodAvailable.id) {
            this.notifyRemoveItem()
          }
          return food.id !== foodAvailable.id;
        })
      })

    } else {
      this.setState({
        inCart: this.state.inCart.map((foodInCart) => {
          if (foodInCart.id === food.id) {
            return { ...foodInCart, quantity: foodInCart.quantity - 1 }
          } else {
            return foodInCart
          }
        })
      })
    }
  }

  removeFromCart = (food) => {
    console.log("remove");
    this.setState({
      inCart: this.state.inCart.filter((item) => {
        if (food.id === item.id) {
          this.notifyRemoveItem()
        }
        return food.id !== item.id
      })
    })
  }

  // ----------------- checkout page functions -----------

  validationCheckout = () => {

    this.setState({
      addressNameError: validator.isEmpty(this.state.addressName) || !validator.isAlpha(this.state.addressName),
      addressNumberError: !validator.isNumeric(this.state.addressNumber) || !validator.isLength(this.state.addressNumber, {min:10, max:10}), 
      addressPincodeError: !validator.isNumeric(this.state.addressPincode) ||  !validator.isLength(this.state.addressPincode, {min:6, max:6}), 
      houseError: false,
      areaNameError: validator.isEmpty(this.state.areaName),

    }, () => {
      if (!this.state.addressNameError &&
        !this.state.addressNumberError &&
        !this.state.addressPincodeError &&
        !this.state.houseError &&
        !this.state.areaNameError) {
        this.setState({
          userAddressName: this.state.addressName,
          userAddressNumber: this.state.addressNumber,
          userAddressPincode: this.state.addressPincode,
          userHouseNo: this.state.houseNo,
          userAreaName: this.state.areaName,
          userLandmark: this.state.landmark,
          addressFlag: true
        })
      }

    })
  }

  updateAddressFlag = () => {
    this.setState({
      addressFlag: false
    }, () => {
    })
  }


  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <Header
            handleChange={this.handleChange}
            signUp={this.signUp}
            login={this.login}
            validation={this.validation}
            loginToSignup={this.loginToSignup}

            loginState={this.state.loginState}
            signUpFlag={this.state.signUpFlag}
            loginPage={this.state.loginPage}
            name={this.state.name}

            nameError={this.state.nameError}
            emailError={this.state.emailError}
            numberError={this.state.numberError}
            passError={this.state.passError}
            varifyError={this.state.varifyError}

            data={data}
            inCart={this.state.inCart}
            state={this.state}
          />

          <Switch>
            <Route path="/"
              render={props => (
                <Restaurants {...props}
                  data={data}
                  getRestaurant={this.getRestaurant}
                />
              )}
              exact
            />

            <Route path="/search"
              render={props => (
                <Search {...props}
                  data={data}
                  addToCart={this.addToCart}

                />
              )}
            />


            <Route path="/view:id"
              render={props => (
                <ViewRestaurant {...props}
                  data={data}
                  addToCart={this.addToCart}
                />
              )}
            />

            <Route path="/cart"
              render={props => (
                <Cart {...props}
                  inCart={this.state.inCart}
                  addToCart={this.addToCart}
                  decreaseQty={this.decreaseQty}
                  removeFromCart={this.removeFromCart}
                  loginState={this.state.loginState}
                  increaseQty={this.increaseQty}
                />
              )}
            />

            <Route path="/checkout"
              render={props => (
                <Checkout {...props}
                  handleChange={this.handleChange}
                  validationCheckout={this.validationCheckout}
                  state={this.state}
                  updateAddressFlag={this.updateAddressFlag}

                />
              )}
            />

          </Switch>

          <Footer />
          <Toaster
            position="top-right"
            reverseOrder={false}
            toastOptions={{
              className: '',
              style: {
                border: '1px solid Green',
                padding: '5px',
                color: 'Green',
                marginTop:"30px",
              },
            }} />
        </BrowserRouter>

      </div>
    );
  }
}

export default App;
