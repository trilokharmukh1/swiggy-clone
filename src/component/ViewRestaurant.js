import React from 'react';
import './../style/ViewRestaurant.css'

export class ViewRestaurant extends React.Component {

    render() {
        let id = Number(this.props.match.params.id.split(":")[1]);
        let restaurant = [];
        restaurant = this.props.data.filter((restaurant) => {
            return restaurant.id === id;
        })

        return (
            <div className='view-restaurants'>
                <div className="main-restaurants container-sd pb-5">

                    <div className="container w-100 d-flex flex-wrap" >
                        <div
                            className="container col-xl-8 col-lg-8 col-md-10 col-sm-11 col-11 flex-sm-column"
                            key={restaurant.id} >

                            <div className='bg-dark w-100 d-flex col-md-12 col-9 col-sm-12 flex-wrap border-bottom pd-3 '>
                                <img className="col-lg-3 col-md-12 col-sm-12 col-12 food-image" src={restaurant[0].image} alt="restaurant" />
                                <div className='col-lg-7'>
                                    <h4 className="mt-2 ms-3 restaurant-name text-white">{restaurant[0].name}</h4>
                                    <div className="ms-3 pe-3 small text-white">
                                        {restaurant[0].description}
                                    </div>
                                    <span className="text-white ms-3 pt-1">✰ {restaurant[0].rating}</span>

                                </div>
                            </div>

                            {
                                restaurant.map((restaurant) => {
                                    return <div className='bg-light d-flex mt-3 pb-4 flex-column-reverse flex-sm-row' key={restaurant.id    }>
                                        <div className='pe-2'>
                                            <h5 className="mt-2 ms-3 restaurant-name text-dart">{restaurant.food}</h5>
                                            <div className="text-black ms-3 pb-1">₹ {restaurant.price}</div>
                                            <div className="ms-3 pe-3 small text-dark food-description">
                                                {restaurant.foodDescription}
                                            </div>
                                            <span className="text-black ms-3 pt-1 mt-2">✰ {restaurant.rating}</span>
                                            <br />

                                            <div className='d-flex justify-content-center'>
                                                <button
                                                    className="btn btn-outline-success ms-3 mt-3 col-6 center d-sm-none d-block"
                                                    onClick={() => { this.props.addToCart(restaurant) }}>
                                                    Add
                                                </button>

                                            </div>

                                        </div>
                                        <div className='col-lg-3 col-md-3 col-sm-3 me-3 ms-3 d-flex flex-column align-items-center'>
                                            <img className="col-12 img-thumbnail m-2 food-image" src={restaurant.foodImage} alt="restaurant" />
                                            <button
                                                className="btn p-0 btn-outline-success center col-11 d-sm-block d-none"
                                                onClick={() => { this.props.addToCart(restaurant) }}>
                                                Add
                                            </button>

                                        </div>
                                        <br />

                                    </div>




                                })
                            }
                        </div>


                    </div>

                </div>
            </div>
        )
    }
}

export default ViewRestaurant