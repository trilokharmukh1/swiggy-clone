import React from 'react';
import { Link } from 'react-router-dom';
import './../style/Header.css';
import arrow from './../images/arrow.svg'
class Search extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            input: "",
        }
    }

    getInput = (event) => {
        this.setState({
            input: event.target.value
        });
    }

    render() {
        let searchResult = "";
        let foodsResult = "";
        if (this.state.input.length > 1) {
            searchResult = this.props.data.filter((item) => {
                let items = item.name.toLowerCase();
                return items.includes(this.state.input.toLowerCase())
            })

            foodsResult = this.props.data.filter((food) => {
                return food.food.toLowerCase().includes(this.state.input.toLocaleLowerCase());
            })
        }

        return (
            <div className="search d-flex flex-column align-items-center">

                <div className='w-100 d-flex justify-content-center mt-5'>
                    <input className="form-control-lg border border-1 col-lg-8 col-md-8 col-sm-9 col-11 center"
                        type="text" placeholder="Search for food"
                        onChange={this.getInput}
                    />
                </div>

                <div className='search-result mt-3 mb-3 container d-flex justify-content-around flex-wrap col-sm-8 col-11 bg-light'>
                    {
                        searchResult.length > 0 &&
                        <div className='h3 p-2 mt-2 search-result-heading w-100 text-center'>Restaurants</div>
                    }

                    {
                        searchResult.length > 0 &&
                        searchResult.map((item) => {
                            return <div className=' col-12 col-sm-10 container m-2 p-3 bg-white  result-container' key={item.id}>
                                <div className='contaner d-flex justify-content-start'>
                                    <img className='col-3 col-sm-2 search-result-image food-restaurant-image' src={item.image} alt="item" />
                                    <div className='ps-3'>
                                        <div className='h5'>{item.name}</div>
                                        <div className='restaurants-description h6 '>{item.description}</div>
                                        <Link to={{ pathname: `/view:${item.id}` }} >
                                            <button className="btn btn-outline-secondary rounded-0 p-0 fs-6 ps-4 pe-4">View</button>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        })

                    }

                    <div className='w-100 bg-white'></div>
                    {
                        foodsResult.length > 0 &&
                        <>
                            <div className='h3 p-2 mt-3 d-block w-100 text-center search-result-heading '>Foods</div>
                            <br />
                        </>
                    }



                    {
                        foodsResult.length > 0 &&
                        foodsResult.map((item) => {
                            return <div className=' col-12 col-sm-10 container m-2 p-3 bg-white result-container' key={item.id}>

                                <Link to={{ pathname: `/view:${item.id}` }} >
                                    <div className='bg-light ps-2 pe-2 restorant-details'>
                                        <div className='m d-2 d-flex col-12 justify-content-between bg-light'>
                                            <div className='text-mute '>By {item.name}</div>
                                            <img className='back-image' src={arrow} alt="back" />
                                        </div>
                                        <div>★ {item.rating}</div>
                                    </div>
                                </Link>

                                <div className='mt-2 contaner d-flex justify-content-start '>
                                    <div className='col-3 col-sm-2 food-restaurant-image'>
                                        <img className='col-12 col-sm-12 search-result-image' src={item.foodImage} alt="item" />
                                    </div>
                                    <div className='ps-3'>
                                        <div className='h6 p-0 m-0'>{item.food}</div>
                                        <div>₹ {item.price}</div>
                                        <div className='restaurants-description food-details'>{item.foodDescription}</div>

                                        <button
                                            className="btn btn-outline-success rounded-0 p-0 fs-6 ps-4 pe-4"
                                            onClick={() => { this.props.addToCart(item) }}
                                        >
                                            Add
                                        </button>

                                    </div>
                                </div>
                            </div>
                        })

                    }





                    {
                        (searchResult === "" && foodsResult === "") &&
                        <div className="message h4 text-mute fw-normal text-center p-3">Type Foods like biryani or cake or ice-cream and we’ll show you matching result.</div>
                    }
                    {
                        ((searchResult.length === 0 && foodsResult.length === 0) && Array.isArray(searchResult) && Array.isArray(foodsResult)) &&
                        <div className="message h4 text-mute fw-normal p-3 text-center">😦 <br /> Hmm, no matches for that.</div>
                    }


                </div>


            </div>
        );
    }
}

export default Search;