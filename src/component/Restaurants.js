import React from "react";
import './../style/Restaurants.css'
import { Link } from 'react-router-dom';

class Restaurants extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="main-restaurants container-sd pb-5">
                <h3 className="ms-5 mt-1 border-bottom pb-2">Restaurants</h3>

                <div className="container w-100 d-flex flex-wrap" >
                    {
                        this.props.data.map((restaurant) => {
                            return <div
                                className="container p-4 col-xl-3 col-lg-3 col-md-4 col-sm-6 col-9 restaurant-container bg-white"
                                key={restaurant.id} >
                                <img className="w-100" src={restaurant.image} alt="restaurant" />
                                <h4 className="mt-2 restaurant-name">{restaurant.name}</h4>

                                <div className="restaurant-description small">
                                    {restaurant.description}
                                </div>

                                <div className="rating mt-2">
                                    <span className="text-white p-1 rate">✰ {restaurant.rating}</span>
                                </div>

                                <div className="view-restaurant-container">
                                    <div className="view-restaurant">
                                        <hr />

                                        <Link to={{ pathname: `/view:${restaurant.id}` }} >
                                            <div className="text-primary text-center">View</div>
                                        </Link>
                                    </div>
                                </div>

                            </div>


                        })
                    }
                </div>

            </div>
        );
    }
}

export default Restaurants;