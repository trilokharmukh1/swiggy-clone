import React from "react";
import './../style/Cart.css';
import empty from './../images/empty.png';
import { Link } from 'react-router-dom';
import Login from "./Login";

class Cart extends React.Component {

    render() {
        let totalPrice = this.props.inCart.reduce((price, item) => {
            price += item.price * item.quantity;
            return price;
        }, 0).toFixed(2);

        let taxes = Number((totalPrice * 0.12).toFixed(2))

        let totalPay = +totalPrice + taxes + 80;

        return (
            <div className="cart pb-4">
                {
                    this.props.inCart.length > 0 &&
                    <div className="container d-flex justify-content-center flex-wrap">
                        <div className="order-detail col-lg-6 col-md-6 col-sm-12 col-10">
                            {
                                this.props.inCart.map((item) => {
                                    return <div className="container bg-white mb-2 col-lg-12 col-md-12 col-sm-12 p-3" key={item.id}>

                                        <div className="d-flex justify-content-between">
                                            <div className="d-flex ms-3">
                                                <img className="img-fluid main-image" src={item.foodImage} alt="food" />
                                                <div className="food-heading ms-3">
                                                    <div className="h5 cart-restaurant-name">{item.name}</div>
                                                    <div className="h6">{item.food}</div>
                                                </div>
                                            </div>
                                            <button className="btn btn-outline-danger btn-sm h-25" onClick={() => { this.props.removeFromCart(item) }}>X</button>
                                        </div>

                                        <div className="food-details mt-3 ps-3 d-flex justify-content-between">
                                            <div className="col-5">
                                                Quantity
                                            </div>
                                            <div className="col-4 quantity-button outline-black mb-3 ms-1 me-1 d-flex justify-content-between align-items-center">
                                                <button
                                                    className="btn btn-sm "
                                                    onClick={() => { this.props.decreaseQty(item) }}
                                                >-</button>

                                                {item.quantity}
                                                <button className="btn btn-sm " onClick={() => { this.props.increaseQty(item) }}>+</button>
                                            </div>
                                            <div className="col-3 text-end">
                                                ₹ {item.price}
                                            </div>
                                        </div>

                                        <div className="bill-details">
                                            <div className="ms-3 mt- d-flex justify-content-between">
                                                <div>Item Total</div>
                                                <div>₹ {item.price * item.quantity}</div>
                                            </div>
                                        </div>

                                    </div>
                                })
                            }
                        </div>

                        <div className="summary-detail container bg-white col-lg-3 col-md-6 col-sm-12 col-10 ms-2 me-2 ">
                            <div className="fw-bold h4 text-center mt-3 pb-2 border-bottom">Order Summary</div>


                            <div className="ms-3 mt-2 d-flex justify-content-between">
                                <div>Total Items</div>
                                <div>{this.props.inCart.length}</div>
                            </div>

                            <div className="ms-3 mt-2 d-flex justify-content-between">
                                <div>Item Price</div>
                                <div>₹ {totalPrice}</div>
                            </div>

                            <div className="ms-3 mt-2 d-flex justify-content-between">
                                <div>Delivery Fee</div>
                                <div>₹ 80</div>
                            </div>

                            <div className="ms-2 ps-1 mt-5 pb-2 border-bottom d-flex justify-content-between">
                                <div>Taxes and Charges</div>
                                <div>₹ {taxes}</div>
                            </div>



                            <div className="ms-3 mt-3 mb-4 fw-bold d-flex justify-content-between">
                                <div>TO PAY</div>
                                <div>₹ {totalPay.toFixed(2)}</div>
                            </div>

                            {
                                this.props.loginState &&
                                <Link to='/checkout'>
                                    <button className="btn btn-success btn-sl btn-block center w-100 mb-3">Checkout</button>
                                </Link>
                            }

                            {
                                !this.props.loginState &&
                                <div className="d-flex justify-content-center mb-2">
                                    <div className="p-0 ps-2 w-25 d-flex align-items-center justify-content-center login-checkout-btn">
                                        <Login />
                                    </div>
                                </div>
                            }


                        </div>
                    </div>
                }

                {
                    this.props.inCart.length === 0 &&
                    <div className="">
                        <div className="p-5 d-flex justify-content-center flex-column align-items-center col-10 bg-white container empty-cart">
                            <div>
                                <img src={empty} alt="empty cart" />
                            </div>
                            <div className="h3 m-2">Your cart is empty</div>
                            <div className="text-center">You can go to home page to view more restaurants</div>

                            <Link to="/" >
                                <button className="login-button btn-sl btn-block center m-3 ps-4 pe-4 fw-bold">SEE RESTAURANTS</button>
                            </Link>
                        </div>
                    </div>
                }

            </div>

        );
    }
}


export default Cart;
