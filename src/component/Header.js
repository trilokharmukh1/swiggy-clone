import React from "react";
import './../style/Header.css';
import { Link } from 'react-router-dom';
import searchIcon from './../images/search.svg';
import cartIcon from './../images/cart.svg';
import Login from './Login';
import loginLogo from './../images/login.svg'


class Header extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <header className="bg-light d-flex justify-content-around">
                <nav className="navbar navbar-expand bg-light ">
                    <div className="nav-container d-flex justify-content-between w-100" >
                        <div className="nav-link d-flex nav-text justify-content-between align-items-center">
                            <Link to="/" >
                                <img className="img-fluid header-logo" src="logo.png" alt="logo" />
                                <span className="ms-1 heading">Swiggy</span>
                            </Link>
                        </div>
                        <div className="navbar-nav list-container" id="navbarNav">
                            <ul className="navbar-nav ul-container justify-content-around">

                                <li className="nav-item">
                                    <Link to="/search">
                                        <div className="nav-link d-flex nav-text justify-content-between align-items-center" aria-current="page" >
                                            <img className="header-icon me-2" src={searchIcon} alt="search" />
                                            Search
                                        </div>
                                    </Link>
                                </li>

                                <li className="nav-item">
                                    <Link to="/cart">
                                        <div className="nav-link d-flex nav-text align-items-center">
                                            <img className="header-icon me-2" src={cartIcon} alt="cart" />
                                            Cart
                                            <div className="cart-size">
                                                {
                                                    this.props.inCart.length > 0 &&
                                                    this.props.inCart.length

                                                }
                                            </div>
                                        </div>
                                    </Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>

                <div className="d-flex">
                <img className="header-icon" src={loginLogo} alt="cart" />

                <Login
                    handleChange={this.props.handleChange}
                    signUp={this.props.signUp}
                    login={this.props.login}
                    loginState={this.props.loginState}
                    validation={this.props.validation}
                    name={this.props.name}

                    nameError={this.props.nameError}
                    emailError={this.props.emailError}
                    numberError={this.props.numberError}
                    passError={this.props.passError}
                    signUpFlag={this.props.signUpFlag}
                    varifyError={this.props.varifyError}


                    loginToSignup={this.props.loginToSignup}
                    loginPage={this.props.loginPage}
                    state={this.props.state}

                />
                </div>


            </header>
        );
    }
}

export default Header;