import React from "react";
import './../style/Checkout.css'
// import { Link } from 'react-router-dom';
import locationIcon from './../images/location.svg'


class Checkout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            upiFlag: true,
            cardFlag: false,
            podFlag: false,
            addressFlag: false,
            paymentPage: false,
        }
    }

    updateAdd = () => {
        if (this.props.state.userAddressName.length > 3) {
            console.log(this.props.state.userAddressName.length);
            this.setState({
                addressFlag: true
            }, (() => {
                console.log(this.state.addressFlag);
            }))
        } else {
            this.setState({
                addressFlag: false
            })
        }
    }

    updatePayment = () => {
        this.setState({
            paymentPage: true
        })
    }

    updateUpiFlag = () => {
        this.setState({
            walletFlag: false,
            upiFlag: true,
            cardFlag: false,
            podFlag: false
        })
    }

    updateCardFlag = () => {
        this.setState({
            walletFlag: false,
            upiFlag: false,
            cardFlag: true,
            podFlag: false
        })
    }

    updatePodFlag = () => {
        this.setState({
            walletFlag: false,
            upiFlag: false,
            cardFlag: false,
            podFlag: true
        })
    }

    render() {

        let totalPrice = this.props.state.inCart.reduce((price, item) => {
            price += item.price * item.quantity;
            return price;
        }, 0).toFixed(2);

        let taxes = Number((totalPrice * 0.12).toFixed(2))

        let totalPay = +totalPrice + taxes + 80;


        let name = this.props.userAddressName;
        return (
            <div className="checkout-container container-sd ">
                <h3 className="ms-5 m-5 mb-3 border-bottom pb-2">Checkout</h3>

                <div className="container pb-5 w-100 d-flex flex-wrap justify-content-around" >

                    <div className="col-11 col-sm-8 mb-2">

                        <div className="me-3 ps-4 pt-4 col-12 pb-3 bg-white">
                            <div className="h5">Delivery address</div>

                            <div className="w-100 ">
                                <div className=" col-12 d-flex justify-content-between">

                                    {
                                        this.state.addressFlag &&
                                        <div className="p-2 ps-4 m-2 border col-sm-8 col-11">

                                            <div className="mb-2">
                                                <img className="location-image" src={locationIcon} alt="location" />
                                                <span className="h5 ps-1"> Your Address</span>
                                            </div>
                                            <div className="ps-4 add-location-text">{this.props.state.userAddressName},</div>
                                            <div className="ps-4 add-location-text">{this.props.state.userAddressNumber},</div>
                                            <div className="ps-4 add-location-text">{this.props.state.userAreaName}, {this.props.state.userLandmark},</div>
                                            <div className="ps-4 add-location-text">{this.props.state.userAddressPincode}</div>

                                            <div className="d-flex justify-content-center">
                                                <button
                                                    className="btn btn-success p-1 mt-3 col-sm-4 col-6 rounded-0 add-address-button"
                                                    // type="button"
                                                    // data-bs-toggle="offcanvas"
                                                    // data-bs-target="#staticBackdrop"
                                                    // aria-controls="staticBackdrop"
                                                    // onClick={() => { this.props.updateAddressFlage() }}
                                                    onClick={() => { this.updatePayment() }}
                                                >
                                                    DELIVER HERE
                                                </button>
                                            </div>
                                        </div>

                                    }


                                    {
                                        !this.state.addressFlag &&
                                        <div className="col-11 col-sm-5">
                                            <div className="col-12 border">
                                                <div className="m-2">
                                                    <img className="location-image" src={locationIcon} alt="location" />
                                                    <span className="h5"> Add New Address</span>
                                                </div>
                                                <div className="ps-4 add-location-text">Add your address for deliver foods</div>

                                                <div className="d-flex justify-content-center m-3">
                                                    <button
                                                        className="btn btn-outline-success p-1 mt-5 col-sm-4 col-6 rounded-0 add-address-button"
                                                        type="button"
                                                        data-bs-toggle="offcanvas"
                                                        data-bs-target="#staticBackdrop"
                                                        aria-controls="staticBackdrop"
                                                        onClick={() => { this.props.updateAddressFlag() }}
                                                    >
                                                        ADD NEW
                                                    </button>

                                                </div>
                                            </div>

                                            <div className="offcanvas offcanvas-start" data-bs-backdrop="static" tabIndex="-1" id="staticBackdrop" aria-labelledby="staticBackdropLabel">
                                                <div className="offcanvas-header justify-content-start">
                                                    <button type="button" className="btn-close" onClick={this.updateAdd} data-bs-dismiss="offcanvas" aria-label="Close" ></button>
                                                    <h5 className="offcanvas-title ms-4" id="staticBackdropLabel">Save delivery address</h5>
                                                </div>

                                                {
                                                    this.props.state.addressFlag &&
                                                    <div className="h4 text-center text-success">Address saved..</div>
                                                }

                                                {
                                                    !this.props.state.addressFlag &&
                                                    <div className="offcanvas-body">


                                                        <input className="form-control form-control-md center pt-10 login-input"
                                                            type="text"
                                                            placeholder="First name"
                                                            name="addressName"
                                                            value={name}
                                                            onChange={this.props.handleChange}
                                                        />
                                                        {
                                                            this.props.state.addressNameError &&
                                                            <div className="form-text text-danger error-label">Name is invalid</div>
                                                        }

                                                        <input className="form-control form-control-md center pt-10 login-input"
                                                            type="text"
                                                            placeholder="Phone number"
                                                            name="addressNumber"
                                                            // value={this.props.state.userAddressNumber}
                                                            onChange={this.props.handleChange}
                                                        />
                                                        {
                                                            this.props.state.addressNumberError &&
                                                            <div className="form-text text-danger error-label">Number is invalid</div>
                                                        }

                                                        <input className="form-control form-control-md center pt-10 login-input"
                                                            type="text"
                                                            placeholder="Pincode (6 digit)"
                                                            name="addressPincode"
                                                            // value={this.props.state.userAddressPincode}
                                                            onChange={this.props.handleChange}
                                                        />
                                                        {
                                                            this.props.state.addressPincodeError &&
                                                            <div className="form-text text-danger error-label">Pincode is invalid</div>
                                                        }

                                                        {/* <input className="form-control form-control-md center pt-10 login-input"
                                                            type="text"
                                                            placeholder="Flat, House no., Building, Company, Apartment"
                                                            name="houseNo"
                                                            // value={this.props.state.userHouseNo}
                                                            onChange={this.props.state.handleChange}
                                                        />
                                                        {
                                                            this.props.state.houseError &&
                                                            <div className="form-text text-danger error-label">Invalid details</div>
                                                        } */}


                                                        <input className="form-control form-control-md center pt-10 login-input"
                                                            type="textArea"
                                                            placeholder="Complete Address (Area, Street, Sector, Village)"
                                                            name="areaName"
                                                            // value={this.props.state.userAreaName}
                                                            onChange={this.props.handleChange}
                                                        />
                                                        {
                                                            this.props.state.areaNameError &&
                                                            <div className="form-text text-danger error-label">Invalid details</div>
                                                        }

                                                        <input className="form-control form-control-md center pt-10 login-input"
                                                            type="text"
                                                            placeholder="Landmark (optional)"
                                                            name="landmark"
                                                            // value={this.props.state.userLandmark}
                                                            onChange={this.props.handleChange}
                                                        />

                                                        <button className="login-button  btn-sl btn-block center w-100 mt-2" onClick={this.props.validationCheckout}>SAVE ADDRESS & PROCEED</button>
                                                    </div>
                                                }

                                            </div>
                                        </div>
                                    }

                                </div>


                            </div>
                        </div>


                        {
                            this.state.paymentPage &&
                            <div className="me-3 p-2 mt-3 col-12 pb-3 pt-4 bg-white">
                                <div className="h5">Choose payment method</div>

                                <div className="payment-option-container col-12 d-flex"          >
                                    <div className="col-3 ps-1 pt-3 me-1 payment-method">
                                            {
                                                this.state.upiFlag &&
                                                <div
                                                    className="bg-white pt-2 pb-2 ps-3 payment"
                                                    onClick={this.updateUpiFlag}>
                                                    UPI
                                                </div>
                                            }

                                            {
                                                !this.state.upiFlag &&
                                                <div
                                                    className="pt-2 pb-2 ps-3 payment"
                                                    onClick={this.updateUpiFlag}>
                                                    UPI
                                                </div>
                                            }

                                        {
                                            this.state.cardFlag &&
                                            <div
                                                className="bg-white pt-2 pb-2 ps-3 payment"
                                                onClick={this.updateCardFlag}>
                                                Credit & Debit cards
                                            </div>
                                        }


                                        {
                                            !this.state.cardFlag &&
                                            <div
                                                className="pt-2 pb-2 ps-3 payment"
                                                onClick={this.updateCardFlag}>
                                                Credit & Debit cards
                                            </div>
                                        }

                                        {
                                            this.state.podFlag &&
                                            <div
                                                className="bg-white pt-2 pb-2 ps-3 payment"
                                                onClick={this.updatePodFlag}>
                                                Pay on Delivery
                                            </div>
                                        }

                                        {
                                            !this.state.podFlag &&
                                            <div
                                                className="pt-2 pb-2 ps-3 payment"
                                                onClick={this.updatePodFlag}>
                                                Pay on Delivery
                                            </div>
                                        }

                                    </div>

                                    <div className="col-9">
                                        {
                                            this.state.walletFlag &&
                                            <div className="col-sm-10 col-11 wallets-payment-option">
                                                <div className="w-100 border pt-3">
                                                    <img className="col-sm-2 col-3 ps-4" src={"https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,h_80/Amazon_Pay_Logos/Pay/logo_pay-primary-fullcolor-positive"} alt="amazon" />
                                                    <div className="ps-1 ps-4 fs-6">Amazon</div>
                                                    <div className="text-danger fs-6 pb-3 ps-4 link-account">LINK ACCOUNT</div>
                                                </div>

                                                <div className="w-100 mt-1 border pt-3">
                                                    <img className="col-sm-2 col-3 ps-4" src={"https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,h_80/phonepe_icon_aca9jf"} alt="amazon" />
                                                    <div className="p-1 ps-4 fs-6">PhonePe (Wallet/UPI/Cards)</div>
                                                    <div className="text-danger pb-3 ps-4 link-account">LINK ACCOUNT</div>
                                                </div>


                                                <div className="w-100 mt-1 border pt-3">
                                                    <img className="col-sm-2 col-3 ps-4" src={"https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,h_80/PaymentLogos/paymentIcons/wallets/paytm"} alt="amazon" />
                                                    <div className="p-1 ps-4">Paytm</div>
                                                    <div className="text-danger pb-3 ps-4 link-account">LINK ACCOUNT</div>
                                                </div>
                                            </div>
                                        }

                                        {
                                            this.state.upiFlag &&
                                            <div className="col-sm-10 col-11 upi-payment-option">
                                                <div className="w-100 border p-3 pt-3">
                                                    <div className="w-100 pb-3 d-sm-flex justify-content-between align-items-center border-secondary border-bottom">
                                                        <div className="col-sm-2 col-3">
                                                            <img
                                                                className="col-12 "
                                                                src={"https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_309,h_84,e_trim/UPI-logo_fjr18d"}
                                                                alt="upi"
                                                            />
                                                        </div>
                                                        <div className="ps-2 pe-2 text-secondary">Transfer money from your bank account using UPI with your registered VPA</div>
                                                    </div>
                                                    <div className="w-100 pt-4 pb-3">
                                                        <div className="h6"> Pay via VPA </div>
                                                        <div className="text-secondary">You must have a Virtual Payment Address</div>
                                                    </div>
                                                    <input
                                                        className="form-control form-control-lg rounded-0 payment-input"
                                                        type="text"
                                                        placeholder="Enter VPA">
                                                    </input>
                                                    <button className="w-100 mt-3 pt-2 pb-2 btn btn-success rounded-0">VERIFY AND PAY</button>
                                                </div>
                                            </div>
                                        }

                                        {
                                            this.state.cardFlag &&
                                            <div className="col-sm-10 col-11 ps-1 card-payment-option">
                                                <div className="w-100 border p-3 pt-3">
                                                    <div className="h5">Add new card</div>

                                                    <div className="d-flex align-items-center pt-1">
                                                        <div className="h6 mt-2">We accept</div>
                                                        <img
                                                            className="col-1 p-1 ms-2"
                                                            src={"https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,e_trim/Visa_lztyeu"}
                                                            alt="card"
                                                        />
                                                        <img
                                                            className="col-1 p-1 ms-2"
                                                            src={"https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,e_trim/Mastercard_wqoea2"}
                                                            alt="card"
                                                        />
                                                        <img
                                                            className="col-1 p-1 ms-2"
                                                            src={"https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,e_trim/Zeta_zybqrc"}
                                                            alt="card"
                                                        />
                                                        <img
                                                            className="col-1 ms-2"
                                                            src={"https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,e_trim/RuPayColoured_oyd73s_soebkd"}
                                                            alt="card"
                                                        />
                                                    </div>

                                                    <div className="input-group w-100">
                                                        <input
                                                            className="form-control form-control-lg rounded-0 w-100 payment-input"
                                                            type="text"
                                                            placeholder="Card number">
                                                        </input>

                                                        <input
                                                            className="form-control form-control-lg rounded-0 w-sm-75 w-50 payment-input"
                                                            type="text"
                                                            placeholder="Validity (MM/YY)">
                                                        </input>

                                                        <input
                                                            className="form-control form-control-lg rounded-0 w-sm-25 w-50  payment-input"
                                                            type="text"
                                                            placeholder="CVV">
                                                        </input>

                                                        <input
                                                            className="form-control form-control-lg rounded-0 col-4 payment-input"
                                                            type="text"
                                                            placeholder="Name on card">
                                                        </input>
                                                    </div>
                                                    <button className="w-100 mt-3 pt-2 pb-2 btn btn-success rounded-0">VERIFY AND PAY</button>
                                                </div>
                                            </div>
                                        }

                                        {
                                            this.state.podFlag &&
                                            <div className="col-sm-10 col-11 ps-3 pod-payment-option">
                                                <div className="w-100 border p-3 pt-3">
                                                    <img
                                                        src={"https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,h_80/PaymentLogos/instruments/4x/Cod"}
                                                        alt="logo"
                                                    />
                                                    <div className="h5 mt-5">Cash</div>
                                                    <div
                                                        className="text-mute">
                                                        Pay cash at the time of delivery. We recommend you use online payments for contactless delivery
                                                    </div>
                                                    <button className="w-100 mt-3 pt-2 pb-2 btn btn-success rounded-0">PLACE ORDER</button>
                                                </div>

                                            </div>
                                        }



                                    </div>

                                </div>
                            </div>
                        }


                    </div>


                    <div className="summary-detail container bg-white col-lg-3 col-md-6 col-sm-12 col-11 ms-2 me-2 ">
                        <div className="fw-bold h4 text-center mt-3 pb-2 border-bottom">Order Summary</div>


                        <div className="ms-3 mt-2 d-flex justify-content-between">
                            <div>Total Items</div>
                            <div>{this.props.state.inCart.length}</div>
                        </div>

                        <div className="ms-3 mt-2 d-flex justify-content-between">
                            <div>Item Price</div>
                            <div>₹ {totalPrice}</div>
                        </div>

                        <div className="ms-3 mt-2 d-flex justify-content-between">
                            <div>Delivery Fee</div>
                            <div>₹ 80</div>
                        </div>

                        <div className="ms-2 ps-1 mt-5 pb-2 border-bottom d-flex justify-content-between">
                            <div>Taxes and Charges</div>
                            <div>₹ {taxes}</div>
                        </div>



                        <div className="ms-3 mt-3 mb-4 fw-bold d-flex justify-content-between">
                            <div>TO PAY</div>
                            <div>₹ {totalPay.toFixed(2)}</div>
                        </div>
                       

                    </div>



                </div>

            </div>
        );
    }
}

export default Checkout;