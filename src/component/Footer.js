import React from "react";
import './../style/Footer.css'
import footerLogo from './../images/footer.webp';

class Footer extends React.Component {
    render() {
        return (

            <footer className="bg-dark d-flex flex-column align-items-center justify-content-around">
                <img src={footerLogo} alt="footer-logo" />

                <div className="footer-right">
                    <div className="copy-right text-white">
                        © Swiggy. All Rights Reserved
                    </div>
                </div>
            </footer>
        );
    }
}

export default Footer;