import React from "react";
import './../style/Header.css'

class Login extends React.Component {

    render() {
        return (
            <div className="login-container ">

                <div className="d-flex justify-content-center">
                    <button className="btn header-login-button" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight">
                        {this.props.loginState &&
                            <div>{this.props.name.charAt(0)} </div>
                        }

                        {!this.props.loginState &&
                            <div className="login-head">Login</div>
                        }


                    </button>
                </div>


                <div className="offcanvas offcanvas-end" data-bs-backdrop="static"  tabIndex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
                    <button type="button" className="btn-close text-reset p-3" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                    <div className="d-flex flex-column p-3">

                        {
                            this.props.loginPage &&
                            <h5 id="offcanvasRightLabel">Login</h5>
                        }

                        {
                            !this.props.loginPage &&
                            <h5 id="offcanvasRightLabel">Sign In</h5>
                        }

                        <div>or
                            {
                                this.props.loginPage &&
                                <span className="text-danger" onClick={this.props.loginToSignup}> create an account</span>
                            }

                            {
                                !this.props.loginPage &&
                                <span className="text-danger" onClick={this.props.loginToSignup}> login to your account</span>
                            }


                        </div>

                    </div>

                    {/* ---------------- login page --------- */}

                    {
                        this.props.loginState &&
                        this.props.loginPage &&
                        <h2 className="text-center">Login Successful</h2>
                    }

                    {
                        this.props.loginPage &&
                        !this.props.loginState &&
                        <div className="offcanvas-body">
                            <input className="form-control form-control-md center login-input"
                                type="text"
                                placeholder="Phone number"
                                name="mobileNumber"
                                onChange={this.props.handleChange}
                            />
                            {
                                this.props.numberError &&
                                <div className="form-text text-danger error-label">Number is invalid</div>
                            }

                            <input className="form-control form-control-md center pt-10 login-input"
                                type="password"
                                placeholder="Password"
                                name="password"
                                onChange={this.props.handleChange}
                            />
                            {
                                this.props.passError &&
                                <div className="form-text text-danger error-label">Password invalid</div>
                            }
                            {
                                this.props.varifyError &&
                                <div className="form-text text-danger error-label">Mobile number or Password is incorrect</div>
                            }


                            <button className="login-button  btn-sl btn-block center w-100 mt-2" onClick={this.props.validation}>LOGIN</button>
                            <small className="form-text text-muted">By clicking on Login, I accept the Terms & Conditions & Privacy Policy</small>
                        </div>
                    }


                    {/* ------------- sign up page --------- */}

                    {
                        this.props.signUpFlag &&
                        !this.props.loginPage &&
                        // !this.props.loginState &&
                        <div className="h2 text-center">Sign Up Successful</div>
                    }


                    {
                        !this.props.loginPage &&
                        !this.props.signUpFlag &&
                        <div className="offcanvas-body">
                            <input className="form-control form-control-md center pt-10 login-input"
                                type="text"
                                placeholder="Phone number"
                                name="mobileNumber"
                                onChange={this.props.handleChange}
                            />
                            {
                                this.props.numberError &&
                                <div className="form-text text-danger error-label">Number is invalid</div>
                            }

                            <input className="form-control form-control-md center pt-10 login-input"
                                type="text"
                                placeholder="First name"
                                name="name"
                                onChange={this.props.handleChange}
                            />
                            {
                                this.props.nameError &&
                                <div className="form-text text-danger error-label">Name is invalid</div>
                            }

                            <input className="form-control form-control-md center pt-10 login-input"
                                type="text"
                                placeholder="Email"
                                name="email"
                                onChange={this.props.handleChange}
                            />
                            {
                                this.props.emailError &&
                                <div className="form-text text-danger error-label">Email is invalid</div>
                            }



                            <input className="form-control form-control-md center pt-10 login-input"
                                type="password"
                                placeholder="Create password"
                                name="password"
                                onChange={this.props.handleChange}
                            />
                            {
                                this.props.passError &&
                                <div className="form-text text-danger error-label">Create password</div>
                            }

                            <button className="login-button  btn-sl btn-block center w-100 mt-2" onClick={this.props.validation}>CONTINUE</button>
                            <small className="form-text text-muted">By creating an account, I accept the Terms & Conditions & Privacy Policy</small>
                        </div>
                    }

                </div>
            </div>
        );
    }
}

export default Login;